function setSessionVariable(windowContext, key, value) {
    "use strict";
    windowContext.sessionStorage.setItem(key, value);
    
}

function removeSessionVariable(windowContext, key) {
    "use strict";
    windowContext.sessionStorage.removeItem(key);
    
}

function getSessionVariable(windowContext, key) {
    "use strict";
    return windowContext.sessionStorage.getItem(key);
}

//loader
function showLoad(show){
	if(show){
		$.blockUI({
			message : '<p style="font-size:24px">Please wait...</p>',
			css : {
				border : 'none',
				padding : '5px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#f2f2f2'
			}
		});
		}
	else{
		$.unblockUI();
		}
}

//Login credential data format.
function loginInfo(username,password) {    
	var login = {"userid": username, "password": password};
	return login;
}

//customer json
function getCust(cust) {
	var customer = {
			name : cust.cust_name,
			email : cust.cust_email,
			phone : cust.cust_phone
	}
	return customer;
}

//update customer
function getUpdateCust(cust) {
	var customer = {
			custId : cust.vr_id,
			name : cust.cust_name,
			phone : cust.cust_phone
	}
	return customer;
}

//get customer id
function getCustId(id) {
	var custId = {
			custId : id
	}
	return custId;
}

//get vr image id
function getImageId(id) {
	var imageId = {
			imageId : id
	}
	return imageId;
}
function logout(){
	sessionStorage.clear();
	window.location.href = "../";
}

function clearCanvas() {
	document.getElementById('update_image').value = "";
	document.getElementById('imageCanvas').clear;
	var imageLoader1 = document.getElementById('upload-image');
	imageLoader1.addEventListener('change', handleImage, false);
	var canvas = document.getElementById('imageCanvas');
	var ctx = canvas.getContext('2d');
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}